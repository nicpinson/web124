//    <meta name="author" content="Nicholas Pinson">
//    <meta name="date" content="3/8/2020">
//    
//    <!-- Adapted from https://javascript30.com - Hold Shift & Check Checkboxes -->

/*
Changes:
Converted an arrow function.
Made that function a Function Declaration instead of remaining anonymous.

New or Useful:
The arguement he passed through querySelectorAll, I didn't realize you could do that.
e's and this's still remain somewhat a mystery.
The inBetween = !inBetween part was weird. I wish he would have explained this more because it seems like a really simple solution.
*/

const checkboxes = document.querySelectorAll('.inbox input[type="checkbox"]');
console.log(checkboxes);

let lastChecked;

function handleCheck(e) {
  // Check if they had the shift key down
  // AND check that they are checking it
  let inBetween = false;
  if (e.shiftKey && this.checked) {
    // go ahead and do what we please
    // loop over every single checkbox
    checkboxes.forEach(checkbox => {
      console.log(checkbox);
      if (checkbox === this || checkbox === lastChecked) {
        inBetween = !inBetween;
        console.log('Starting to check them in between!');
      }

      if (inBetween) {
        checkbox.checked = true;
      }
    });
  }

  lastChecked = this;
}


/* Wes Bos Code
checkboxes.forEach(checkbox => checkbox.addEventListener('click', handleCheck));
*/


// Created Function Declaration instead of using an anonymous function
function checkEventListener(checkbox) {
    checkbox.addEventListener("click", handleCheck);
}

// Loop through NodeList and run function on each of them
checkboxes.forEach(checkEventListener);