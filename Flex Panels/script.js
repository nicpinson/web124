/*
  <meta name="author" content="Nicholas Pinson">
  <!-- Adapted from https://javascript30.com - Flex Panel Gallery -->

Changes:
-Converted the arrow functions (which still seem foreign to me, need more practice)

New to me:
-forEach
-transitionend events
-e/event
-this
*/

const panels = document.querySelectorAll('.panel');
console.log(panels);
//okay, so I'm getting a node list containing all the panels

function toggleOpen() {
  //console.log('Hello');
  this.classList.toggle('open');
}

function toggleActive(e) {
  console.log(e.propertyName);
  if (e.propertyName.includes('flex')) {
    this.classList.toggle('open-active');
  }
}
/*
panels.forEach(panel => panel.addEventListener('click', toggleOpen));
panels.forEach(panel => panel.addEventListener('transitionend', toggleActive));
*/


let panelClick = function(panel) {
    panel.addEventListener('click', toggleOpen);
}

let panelTransition = function(panel) {
    panel.addEventListener('transitionend', toggleActive);
}

panels.forEach(panelClick);
panels.forEach(panelTransition);