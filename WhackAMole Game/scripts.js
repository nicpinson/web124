/*

<meta name="author" content="Nicholas Pinson">
<meta name="date" content="2/3/2020">
<!-- Adapted from https://javascript30.com - Whack A Mole Game -->
<!-- New concepts, or things I felt were beneficial to review: 

            function randomTime(min, max)
            const time = randomTime(200, 1000);
            hole.classList.add('up');
            hole.classList.remove('up');
            setTimeout(() => timeUp = true, 13900)

function bonk(e) {
            if(!e.isTrusted) return; // cheater!
            score++;
            this.parentNode.classList.remove('up');
            scoreBoard.textContent = score;



*/

            const holes = document.querySelectorAll('.hole');
            const scoreBoard = document.querySelector('.score');
            const moles = document.querySelectorAll('.mole');
            let lastHole;
            let timeUp = false;
            let score = 0;


            function randomTime(min, max) {
            return Math.round(Math.random() * (max - min) + min);
            }

            function randomHole(holes) {
            const idx = Math.floor(Math.random() * holes.length);
            const hole = holes[idx];
            if (hole === lastHole) {
              console.log('Ah nah thats the same one bud');
              return randomHole(holes);
            }
            lastHole = hole;
            return hole;
            }

            function peep() {
            const time = randomTime(200, 1000);
            const hole = randomHole(holes);
            hole.classList.add('up');
            setTimeout(() => {
              hole.classList.remove('up');
              if (!timeUp) peep();
            }, time);
            }

            function startGame() {
            scoreBoard.textContent = 0;
            timeUp = false;
            score = 0;
            peep();
            setTimeout(() => timeUp = true, 13900)
//Here is something I added in his code            
            changePokeball();
            playAudio()
            }

            function bonk(e) {
            if(!e.isTrusted) return; // cheater!
            score++;
            this.parentNode.classList.remove('up');
            scoreBoard.textContent = score;
//Here is something I added in his code
            changePokeball();
            }

            moles.forEach(mole => mole.addEventListener('click', bonk));



//Replacing the onClick="startGame()" JavaScript to avoid having anything inline with my HTML
document.getElementById("startbutton").addEventListener("click", startGame);


//My main contribution: Change Pokeball cursor based on score

//let totalPoints = document.getElementById("total").textContent;
let pokeCursor = document.getElementsByClassName("pokeball");
let currentPokeball = pokeCursor[0];

//Just testing that things are returning what I want them to return
//console.log(totalPoints);
//console.log(currentPokeball);

//This needs to be replaced, but not sure atm how to call on my changePokeball() function
//if (totalPoints == 0) {
//    changePokeball();
//}

//Maybe something like this?
//document.getElementById("totalPoints").addEventListener("onchange", changePokeball);


//I want to use the other changePokeball() function eventually, but I've tested this and I know it works. I just don't know how to loop and check to see if the score has changed.

//function changePokeball() {
//    if (totalPoints >= 0) {
//        currentPokeball.classList.remove("pokeball");
//        currentPokeball.classList.add("greatball");
//}   
//
//}



// #^#^#^#^#^# This is the function I ultimately want to use! #^#^#^#^#^#

//If the count goes over 4, I want a Greatball cursor, if it goes over 9 I want an Ultraball cursor. It will reapply the Pokeball class each time you hit the Start button, as the textContent is reset to 0.



function changePokeball() {
    let totalPoints = document.getElementById("total").textContent;
    if (totalPoints >= 10) {
        currentPokeball.classList.remove("greatball");
        currentPokeball.classList.add("ultraball");
}   else if (totalPoints >= 5) {
        currentPokeball.classList.remove("pokeball");
        currentPokeball.classList.add("greatball");
}   else {
        currentPokeball.classList.remove("ultraball")
        currentPokeball.classList.remove("greatball")
        currentPokeball.classList.add("pokeball")
}

}


//Add some music because why not and it's something I've never used before.
var battleMusic = document.getElementById("battlemusic");

function playAudio() {
  battleMusic.play();
}



















