//  <meta name="author" content="Nicholas Pinson">
//  <meta name="date" content="2/17/2020">
//  <!-- Adapted from https://javascript30.com - Konami Code / Key Sequence Detection -->

// Wes had a really unique way of implementing his idea with the splice method that I wouldn't have thought of. Quickly (not manually) modifying arrays is an area that I need improvement.

// I updated his code so the secret code is "starfox" and when typed it will play the Starfox Theme. If you click on the ship, it will play a quote from the robot, ROB, that pilots the ship. 

const pressed = [];
const secretCode = 'starfox';

window.addEventListener('keyup', (e) => {
  console.log(e.key);
  pressed.push(e.key);
  pressed.splice(-secretCode.length - 1, pressed.length - secretCode.length);
  if (pressed.join('').includes(secretCode)) {
    console.log('ALL REPORT!');
    playAudio();
  }
  console.log(pressed);
});


// Play Starfox Theme
var introMusic = document.getElementById("intromusic");

function playAudio() {
  introMusic.play();
}

// Play ROB64 Quote
document.getElementById("greatfox").addEventListener("click", playQuote);

var robQuote = document.getElementById("robquote");
function playQuote() {
    robQuote.play();
}


////////////////// This part is from Cornify, implemented through CSS instead
//    // Add a nice hover wigggle.
//    img.style.transition = "all .1s linear";
//
//    div.onmouseover = function() {
//        var size = 1 + Math.round(Math.random()*10)/100;
//        var angle = Math.round(Math.random()*20-10);
//        var result = "rotate("+angle+"deg) scale("+size+","+size+")";
//        img.style.transform = result;
//        img.style.MozTransform = result;
//        img.style.webkitTransform = result;
//    };
//
//    div.onmouseout = function() {
//        var size = .9+Math.round(Math.random()*10)/100;
//        var angle = Math.round(Math.random()*6-3);
//        var result = "rotate("+angle+"deg) scale("+size+","+size+")";
//        img.style.transform = result;  
//        img.style.MozTransform = result;
//        img.style.webkitTransform = result;
//    };